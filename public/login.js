// login.js
// Método para rotar un carácter
function rotarCaracter(caracter, rotacion) {
    const codigo = caracter.charCodeAt(0);
    let nuevoCodigo;
    if (codigo >= 65 && codigo <= 90) { // Mayúsculas
        nuevoCodigo = ((codigo - 65 + rotacion) % 26) + 65;
    } else if (codigo >= 97 && codigo <= 122) { // Minúsculas
        nuevoCodigo = ((codigo - 97 + rotacion) % 26) + 97;
    } else {
        return caracter;
    }
    return String.fromCharCode(nuevoCodigo);
}

// Método para ofuscar una cadena
function ofuscarCadena(cadena) {
    const reemplazos = {'a': '4', 'e': '3', 'i': '1', 'o': '0', 'u': '9', 'l': '7', 't': '+', 's': '$'};
    const rotacion = 5; // Rotación fija
    let cadenaOfuscada = cadena.split('').map(c => rotarCaracter(c, rotacion)).map(c => reemplazos[c.toLowerCase()] || c).join('');
    return btoa(encodeURIComponent(cadenaOfuscada)); // Codificamos en Base64
}

// Método para revertir la rotación de un carácter
function revertirRotacion(caracter, rotacion) {
    const codigo = caracter.charCodeAt(0);
    let nuevoCodigo;
    if (codigo >= 65 && codigo <= 90) { // Mayúsculas
        nuevoCodigo = ((codigo - 65 - rotacion + 26) % 26) + 65;
    } else if (codigo >= 97 && codigo <= 122) { // Minúsculas
        nuevoCodigo = ((codigo - 97 - rotacion + 26) % 26) + 97;
    } else {
        return caracter;
    }
    return String.fromCharCode(nuevoCodigo);
}

// Método para desofuscar una cadena
function desofuscarCadena(cadenaOfuscada) {
    const reemplazosInversos = {'4': 'a', '3': 'e', '1': 'i', '0': 'o', '9': 'u', '7': 'l', '+': 't', '$': 's'};
    const rotacion = 5; // Rotación fija
    let cadenaDecodificada = decodeURIComponent(atob(cadenaOfuscada));
    let cadenaDesofuscada = cadenaDecodificada.split('').map(c => reemplazosInversos[c] || c).map(c => revertirRotacion(c, rotacion)).join('');
    return cadenaDesofuscada;
}

document.getElementById('loginForm').addEventListener('submit', function(event) {
    event.preventDefault();

    const usuario = document.getElementById('usuario').value;
    const contrasena = document.getElementById('contrasena').value;

    const usuarioOfuscado = ofuscarCadena(usuario);
    const contrasenaOfuscada = ofuscarCadena(contrasena);

    const cuentasOfuscadas = {
        'SCVDMyVBOXhmdw==': 'V2o3ZnElMkJIanhmdzIwMjM=',
        'MCUyQnglQzMlQTklMjBIZndxJTJCeA==': 'SGZ3cSUyQnglMjRmNG4xZjEyMw==',
        'SG1md3Fk': 'SG1md3FkWGYlMjR5ZjEyMw==',
        'MWYlMjRk': 'MWYlMjRkSCUyQjlueSUyQjIwMjM=',
        'UXpoZA==': 'UXpoZEp4eXdqcXFm',
        'UmZybiUyMFlqd2o=': 'WWp3aiUyNGZobnJuaiUyNHklMkI=',
        'SnJucW5mJTI0JTJC': 'SnJuOWZ4aHpmMjAyMw==',
        'SGpobnFuZg==': 'SGpobkYlQzMlQjElMkIlMjR6ajQlMkI=',
        'Rndk': 'RndkTiUyNDRuanclMjQlMkIyMw=='
    };

    const regalosOfuscados = {
        'MWYlMjRk': 'Rndk',
        'Rndk': 'SGpobnFuZg==',
        'SGpobnFuZg==': 'SG1md3Fk',
        'SG1md3Fk': 'UXpoZA==',
        'UXpoZA==': 'SnJucW5mJTI0JTJC',
        'SnJucW5mJTI0JTJC': 'MCUyQnglQzMlQTklMjBIZndxJTJCeA==',
        'MCUyQnglQzMlQTklMjBIZndxJTJCeA==': 'SCVDMyVBOXhmdw==',
        'SCVDMyVBOXhmdw==': 'UmZybiUyMFlqd2o=',
        'UmZybiUyMFlqd2o=': 'MWYlMjRk'
    };

    if (cuentasOfuscadas[usuarioOfuscado] && cuentasOfuscadas[usuarioOfuscado] === contrasenaOfuscada) {
        $('#loginBox').hide();
        let counter = 5;
        const interval = setInterval(function() {
            if (counter === 0) {
                clearInterval(interval);
                showFireworks();
                setTimeout(function() {
                    $('#resultadoSorteo').show().html(`<div>
                                                            <h1>Bienvenido ${usuario}! </h1>
                                                            <br> <br>
                                                            <h2>Le das regalo a <br>
                                                                <b>${desofuscarCadena(regalosOfuscados[usuarioOfuscado])}</b>.</h2>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <div id="listPerson">
                                                            <b>---- Lista de personas ----</b>
                                                            <br>
                                                            José Carlos - Papá de Dany
                                                            <br>
                                                            Charly - Hermano de Dany
                                                            <br>
                                                            Ari - Pareja de Charly
                                                            <br>
                                                            Mami Tere - Abuelita de César
                                                            <br>
                                                            Lucy - Mamá de César
                                                            <br>
                                                            Cecilia - Hermana de César
                                                            <br>
                                                            Emiliano - Hermano de César
                                                        </div>
                                                    </div>`);
                }, 1000);
            } else {
                $('#resultadoSorteo').show().html(`<div class="alert alert-warning" role="alert">
                                                    Preparándose para revelar en ${counter}...
                                                   </div>`);
                counter--;
            }
        }, 1000);
    } else {
        alert('Usuario o contraseña incorrectos');
    }
});

function showFireworks() {
    for (let i = 0; i < 90; i++) {
        const firework = document.createElement('div');
        firework.classList.add('firework');
        firework.style.left = `${Math.random() * 100}%`;
        firework.style.top = `${Math.random() * 100}%`;
        document.body.appendChild(firework);
    }
    for (let i = 0; i < 90; i++) {
        const fireworkBlue = document.createElement('div');
        fireworkBlue.classList.add('fireworkBlue');
        fireworkBlue.style.left = `${Math.random() * 100}%`;
        fireworkBlue.style.top = `${Math.random() * 100}%`;
        document.body.appendChild(fireworkBlue);
    }
    for (let i = 0; i < 90; i++) {
        const fireworkYellow = document.createElement('div');
        fireworkYellow.classList.add('fireworkYellow');
        fireworkYellow.style.left = `${Math.random() * 100}%`;
        fireworkYellow.style.top = `${Math.random() * 100}%`;
        document.body.appendChild(fireworkYellow);
    }
}
